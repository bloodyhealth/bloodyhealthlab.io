var accordion = document.getElementsByClassName("faq-title");
var i;

for (i = 0; i < accordion.length; i++) {
  accordion[i].addEventListener("click", function () {
    this.classList.toggle("active");
    var faqitem = this.nextElementSibling;
    if (faqitem.style.display === "block") {
      faqitem.style.display = "none";
    } else {
      faqitem.style.display = "block";
    }
  });
}
